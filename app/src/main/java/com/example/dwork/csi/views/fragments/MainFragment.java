package com.example.dwork.csi.views.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dwork.csi.R;
import com.example.dwork.csi.models.NewsModel;
import com.example.dwork.csi.presenters.NewsPresenter;
import com.example.dwork.csi.utils.FontUtil;
import com.example.dwork.csi.views.adapters.RecyclerAdapter;

import rx.Subscriber;
import rx.android.observables.ViewObservable;
import rx.functions.Action1;

/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class MainFragment extends Fragment {

    private TextView title_tv;
    private TextView status_tv;
    private TextView create_hint_tv;
    private TextView create_date_tv;
    private TextView register_hint_tv;
    private TextView register_date_tv;
    private TextView solve_hint_tv;
    private TextView solve_date_tv;
    private TextView solver_hint_tv;
    private TextView solver_name_tv;
    private TextView main_text_tv;

    private RecyclerView recyclerView;
    private RecyclerAdapter mAdapter;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        title_tv = (TextView) rootView.findViewById(R.id.title_tv);
        status_tv = (TextView) rootView.findViewById(R.id.status_tv);
        create_hint_tv = (TextView) rootView.findViewById(R.id.create_hint_tv);
        create_date_tv = (TextView) rootView.findViewById(R.id.create_date_tv);
        register_hint_tv = (TextView) rootView.findViewById(R.id.register_hint_tv);
        register_date_tv = (TextView) rootView.findViewById(R.id.register_date_tv);
        solve_hint_tv = (TextView) rootView.findViewById(R.id.solve_hint_tv);
        solve_date_tv = (TextView) rootView.findViewById(R.id.solve_date_tv);
        solver_hint_tv = (TextView) rootView.findViewById(R.id.solver_hint_tv);
        solver_name_tv = (TextView) rootView.findViewById(R.id.solver_name_tv);
        main_text_tv = (TextView) rootView.findViewById(R.id.main_text_tv);

        ViewObservable.clicks(title_tv, false).subscribe(clickAction);
        ViewObservable.clicks(status_tv, false).subscribe(clickAction);
        ViewObservable.clicks(create_hint_tv, false).subscribe(clickAction);
        ViewObservable.clicks(create_date_tv, false).subscribe(clickAction);
        ViewObservable.clicks(register_hint_tv, false).subscribe(clickAction);
        ViewObservable.clicks(register_date_tv, false).subscribe(clickAction);
        ViewObservable.clicks(solve_hint_tv, false).subscribe(clickAction);
        ViewObservable.clicks(solve_date_tv, false).subscribe(clickAction);
        ViewObservable.clicks(solver_hint_tv, false).subscribe(clickAction);
        ViewObservable.clicks(solver_name_tv, false).subscribe(clickAction);
        ViewObservable.clicks(main_text_tv, false).subscribe(clickAction);

        title_tv.setTypeface(FontUtil.getRobotoBoldTypeFace(getContext()));
        status_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        create_hint_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        create_date_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        register_hint_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        register_date_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        solve_hint_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        solve_date_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        solver_hint_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        solver_name_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));
        main_text_tv.setTypeface(FontUtil.getRobotoMediumTypeFace(getContext()));

        recyclerView = (RecyclerView) rootView.findViewById(R.id.pictures_rv);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new RecyclerAdapter(getContext(),clickAction);
        recyclerView.setAdapter(mAdapter);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle(getContext().getString(R.string.load));
        progressDialog.setMessage(getContext().getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();

        new NewsPresenter(getContext(), subscriber).requestForNews();

        return rootView;
    }


    Subscriber<NewsModel> subscriber = new Subscriber<NewsModel>() {
        @Override
        public void onCompleted() {
            Log.d("tag", "onCompleted()");
        }

        @Override
        public void onError(Throwable e) {
            Log.d("tag", e.getMessage());
        }

        @Override
        public void onNext(NewsModel news) {
            showNew(news);
        }
    };


    public void showNew(NewsModel new_) {

        progressDialog.dismiss();

        title_tv.setText(new_.getTitle());
        status_tv.setText(new_.getStatus());
        create_hint_tv.setText(new_.getCreate_hint());
        create_date_tv.setText(new_.getCreate_date());
        register_hint_tv.setText(new_.getRegister_hint());
        register_date_tv.setText(new_.getRegister_date());
        solve_hint_tv.setText(new_.getSolve_hint());
        solve_date_tv.setText(new_.getSolve_date());
        solver_hint_tv.setText(new_.getSolver_hint());
        solver_name_tv.setText(new_.getSolver_name());
        main_text_tv.setText(new_.getMain_text());

        mAdapter.setUrlList(new_.getUrlList());
        mAdapter.notifyDataSetChanged();
    }

    Action1<View> clickAction = view -> {
        String id_ = view.getResources().getResourceName(view.getId());
        String id_name = id_.substring(id_.lastIndexOf("/") + 1);
        Toast.makeText(getContext(), id_name, Toast.LENGTH_SHORT).show();
    };

}
