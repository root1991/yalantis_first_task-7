package com.example.dwork.csi.presenters;

import android.content.Context;

import com.example.dwork.csi.models.NewsModel;
import com.example.dwork.csi.services.RequestService;

import java.util.concurrent.TimeUnit;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Korzch Alexandr on 3/13/2016.
 */
public class NewsPresenter {

    private Context context;
    private Subscriber<NewsModel> subscriber;

    public NewsPresenter(Context context, Subscriber<NewsModel> subscriber) {
        this.subscriber = subscriber;
        this.context = context;
    }

    public void requestForNews(){

        new RequestService(context).getNew()
                .retry(5)
                .timeout(5, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subscriber);
    }
}
